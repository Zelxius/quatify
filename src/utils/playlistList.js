export const playlistList = [
  {
    title: 'Canciones que te gustan',
    link: 'https://open.spotify.com/collection/tracks',
    image: new URL('../assets/played-playlists/likedsongs.png', import.meta.url).href
  },
]
