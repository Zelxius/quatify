export const suggestedPlaylists = [
  {
    title: 'Mamivan',
    caption: 'Tus canciones favoritas de pop te acompañan a donde quiera que vayas.',
    image: new URL('../assets/suggested-playlists/mamivan-cover.png', import.meta.url).href
  },
  {
    title: 'Pal tráfico',
    caption: '¿A vuelta de rueda? Con esta playlist el camino será más ligero.',
    image: new URL('../assets/suggested-playlists/pal-trafico-cover.png', import.meta.url).href
  },
  {
    title: 'Ruleteando',
    caption: 'El icuiricui, el matalacachimba y el ruletero.',
    image: new URL('../assets/suggested-playlists/taxi-ruleteando-cover.png', import.meta.url).href
  },
  {
    title: 'Viaje en carretera nostalgia',
    caption: 'Recorre los kilómetros con estos cantables clásicos para el camino.',
    image: new URL('../assets/suggested-playlists/viaje-en-carretera-cover.png', import.meta.url).href
  },
]
