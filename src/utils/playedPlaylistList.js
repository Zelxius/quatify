export const playedPlaylist = [
  {
    title: 'Canciones que te gustan',
    image: new URL('../assets/played-playlists/likedsongs.png', import.meta.url).href
  },
  {
    title: 'Fiesta',
    image: new URL('../assets/played-playlists/fiesta-spotify-cover.jpeg', import.meta.url).href
  },
  {
    title: 'Mañanitas',
    image: new URL('../assets/played-playlists/mañanitas-cover.png', import.meta.url).href
  },
  {
    title: 'Favoritas',
    image: new URL('../assets/played-playlists/favoritas-spotify-cover.png', import.meta.url).href
  },
  {
    title: 'Cumbia',
    image: new URL('../assets/played-playlists/cumbia-spotify-cover.png', import.meta.url).href
  },
  {
    title: 'Banda bailar',
    image: new URL('../assets/played-playlists/banda-para-bailar.png', import.meta.url).href
  },
]
