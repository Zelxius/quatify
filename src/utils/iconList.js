export const iconsMedia = [
  {
    description: "Activar aleatorio",
    icon: "shuffle",
    size: "sm"
  },
  {
    description: "Anterior",
    icon: "skip_previous",
    size: "sm"
  },
  {
    description: "Pausar",
    icon: "play_circle_filled",
    size: "lg"
  },
  {
    description: "Siguiente",
    icon: "skip_next",
    size: "sm"
  },
  {
    description: "Activar repetir",
    icon: "replay",
    size: "sm"
  }
]

export const iconsVolume = [
  {
    description: "Letra",
    icon: "mic",
    size: "sm"
  },
  {
    description: "Cola",
    icon: "horizontal_split",
    size: "sm"
  },
  {
    description: "Conectar a un dispositivo",
    icon: "phonelink",
    size: "sm"
  },
  {
    description: "Silenciar",
    icon: "volume_down",
    size: "sm"
  }
]

export const iconsFooter = [
  {
    description: "Guardar en tu biblioteca",
    icon: "favorite_border",
    size: "sm"
  },
  {
    description: "Texto desconocido",
    icon: "remove_circle_outline",
    size: "sm"
  },
  {
    description: "Imagen en imagen",
    icon: "devices",
    size: "sm"
  }
]
