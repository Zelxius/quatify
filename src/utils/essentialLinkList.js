export const essentialLinks = [
  {
    title: 'Inicio',
    icon: 'home',
    link: 'https://open.spotify.com'
  },
  {
    title: 'Buscar',
    icon: 'search',
    link: 'https://open.spotify.com/search'
  },
  {
    title: 'Tu biblioteca',
    icon: 'playlist_play',
    link: 'https://open.spotify.com/collection'
  }
]
